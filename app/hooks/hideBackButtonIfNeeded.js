import React, { useEffect, useState } from 'react';

import HeaderBackButton from '../components/HeaderBackButton';

export default (props) => {
    const { route, navigation } = props;
    const [backButtonIsHidden, setBackButtonIsHidden] = useState(
        route.params.preventBack,
    );

    useEffect(() => {
        const { preventBack } = route.params;
        navigation.setOptions({
            headerLeft: preventBack
                ? null
                : (headerProps) => <HeaderBackButton {...headerProps} />,
        });

        setBackButtonIsHidden(preventBack);
    }, [route.params.preventBack]);

    return backButtonIsHidden;
};
