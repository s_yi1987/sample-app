import { SET_BRANCH } from '../const/branch';

export const defaultState = {
    branch: null,
};

export default (state = defaultState, action) => {
    if (action.type === SET_BRANCH) {
        return {
            ...state,
            branch: { ...state.branch, ...action.branch },
        };
    }

    return state;
};
