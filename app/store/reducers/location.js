import { SET_LOCATION } from '../const/location';

export const defaultState = {
    location: null,
};

export default (state = defaultState, action) => {
    if (action.type === SET_LOCATION) {
        return {
            ...state,
            location: { ...state.location, ...action.location },
        };
    }

    return state;
};
