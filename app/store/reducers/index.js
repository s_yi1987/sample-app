import { combineReducers } from 'redux';

import branch from './branch';
import location from './location';

export default combineReducers({
    branch,
    location,
});
