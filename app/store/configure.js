import { createStore } from 'redux';

import combinedReducers from './reducers';

export default (preloadedState) =>
    createStore(combinedReducers, preloadedState);
