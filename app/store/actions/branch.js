import { SET_BRANCH } from '../const/branch';

// eslint-disable-next-line import/prefer-default-export
export const setBranch = (branch) => ({
    type: SET_BRANCH,
    branch,
});
