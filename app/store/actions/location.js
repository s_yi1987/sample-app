import { SET_LOCATION } from '../const/location';

// eslint-disable-next-line import/prefer-default-export
export const setLocation = (location) => ({
    type: SET_LOCATION,
    location,
});
