import React from 'react';

const SceneFactoryContext = React.createContext();

export default SceneFactoryContext;
