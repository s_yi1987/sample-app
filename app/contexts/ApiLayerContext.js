import React from 'react';

const ApiLayerContext = React.createContext();

export default ApiLayerContext;
