import React, { Component } from 'react';
import { View, StyleSheet, ViewPropTypes } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import PropTypes from 'prop-types';

import Colors from '../../config/Colors';

export default class AnimatedTextTab extends Component {
    shouldComponentUpdate(newProps) {
        const { label, index } = this.props;
        return label !== newProps.label || index !== newProps.index;
    }

    renderInactiveText = (opacityAnim) => {
        const { fontFamily, textColor, fontSize } = this.props;

        return this.renderText({
            fontFamily,
            color: textColor,
            fontSize,
            active: false,
            opacity: Animated.not(opacityAnim),
        });
    };

    renderActiveText = (opacityAnim) => {
        const {
            activeFontFamily,
            activeTextColor,
            activeFontSize,
        } = this.props;

        return this.renderText({
            fontFamily: activeFontFamily,
            color: activeTextColor,
            fontSize: activeFontSize,
            active: true,
            opacity: opacityAnim,
        });
    };

    renderText = ({ fontFamily, color, fontSize, opacity }) => {
        const { label } = this.props;

        return (
            <Animated.Text
                style={{
                    fontFamily,
                    color,
                    fontSize,
                    opacity,
                }}
            >
                {label}
            </Animated.Text>
        );
    };

    onPress = () => {
        const { onPress } = this.props;
        if (onPress) {
            onPress();
        }
    };

    render() {
        const {
            fontFamily,
            activeFontFamily,
            textColor,
            activeTextColor,
            fontSize,
            activeFontSize,
            onPress,
            label,
            scrollValue,
            index,
            style,
            buttonStyle,
            ...rest
        } = this.props;
        const opacityAnim = Animated.cond(
            Animated.eq(Animated.round(scrollValue), index),
            1,
            0,
        );
        return (
            <View style={style} {...rest}>
                <TouchableOpacity
                    style={buttonStyle}
                    onPress={this.onPress}
                    activeOpacity={1}
                >
                    <View style={styles.inactiveTextContainer}>
                        {this.renderInactiveText(opacityAnim)}
                    </View>
                    <View style={styles.activeTextContainer}>
                        {this.renderActiveText(opacityAnim)}
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    inactiveTextContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    activeTextContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

AnimatedTextTab.propTypes = {
    style: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    buttonStyle: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    fontFamily: PropTypes.string,
    activeFontFamily: PropTypes.string,
    textColor: PropTypes.string,
    activeTextColor: PropTypes.string,
    fontSize: PropTypes.number,
    activeFontSize: PropTypes.number,
    onPress: PropTypes.func,
    label: PropTypes.string,
    scrollValue: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
};

AnimatedTextTab.defaultProps = {
    style: undefined,
    buttonStyle: undefined,
    fontFamily: undefined,
    activeFontFamily: undefined,
    textColor: Colors.BLACK,
    activeTextColor: Colors.GOLDEN_ROD,
    fontSize: wp('4%'),
    activeFontSize: wp('4%'),
    onPress: undefined,
    label: undefined,
};
