import React, { Component } from 'react';
import { StyleSheet, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import DynamicWidthTabBar from '../DynamicWidthTabBar';
import AnimatedTextTab from './AnimatedTextTab';
import withDataRelay from '../DataRelayer';

import Colors from '../../config/Colors';
import { getCombinedStyles } from '../../api/util';

const DataAnimatedTextTab = withDataRelay(AnimatedTextTab, 'onPress');

export default class AnimatedTextTabBar extends Component {
    renderTab = ({ tab, index, isTabActive, onPress, onLayout }) => {
        const { tabHeight, tabMargin, scrollValue, tabs } = this.props;
        let marginRight = 0;
        if (tabs && index === tabs.length - 1) {
            marginRight = tabMargin;
        }
        const style = { marginLeft: tabMargin, marginRight };
        const buttonStyle = { height: tabHeight };

        return (
            <DataAnimatedTextTab
                key={tab.key}
                style={style}
                buttonStyle={buttonStyle}
                onLayout={onLayout}
                onPress={this.onTabPress}
                data={{ onPress, index }}
                label={tab.label}
                isActive={isTabActive}
                scrollValue={scrollValue}
                index={index}
            />
        );
    };

    onTabPress = ({ onPress, index }) => {
        if (onPress) {
            onPress(index);
        }
    };

    render() {
        const {
            underlineColor,
            underlineHeight,
            tabHeight,
            style,
            tabBarUnderlineStyle,
            ...rest
        } = this.props;
        const tabBarStyle = getCombinedStyles(styles.tabBar, style);
        const underlineStyle = getCombinedStyles(
            styles.indicator,
            tabBarUnderlineStyle,
        );
        return (
            <DynamicWidthTabBar
                {...rest}
                renderTab={this.renderTab}
                style={tabBarStyle}
                tabsContainerStyle={styles.tabsContainerStyle}
                underlineStyle={underlineStyle}
            />
        );
    }
}

const BAR_HEIGHT = hp('6%');

AnimatedTextTabBar.propTypes = {
    style: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    tabHeight: PropTypes.number,
    tabMargin: PropTypes.number,
    underlineColor: PropTypes.string,
    underlineHeight: PropTypes.number,
    tabs: PropTypes.array,
    tabBarUnderlineStyle: PropTypes.oneOfType([
        ViewPropTypes.style,
        PropTypes.array,
    ]),
    scrollValue: PropTypes.object,
};

AnimatedTextTabBar.defaultProps = {
    style: undefined,
    tabHeight: BAR_HEIGHT,
    tabMargin: wp('8%'),
    underlineColor: Colors.GOLDEN_ROD,
    underlineHeight: hp('1%'),
    tabs: undefined,
    tabBarUnderlineStyle: undefined,
    scrollValue: undefined,
};

const styles = StyleSheet.create({
    indicator: {
        height: wp('1.1%'),
        backgroundColor: Colors.GOLDEN_ROD,
    },
    tabBar: {
        height: BAR_HEIGHT,
        borderBottomWidth: 1,
        borderBottomColor: Colors.PALE_GREY,
    },
    tabsContainerStyle: {
        height: BAR_HEIGHT,
        alignSelf: 'center',
    },
});
