import React, { Component } from 'react';
import _ from 'lodash';

const withDataRelay = (PassedComponent, callbackName, metadataName = 'data') =>
    class DataRelayer extends Component {
        constructor(props) {
            super(props);

            const callbackNames = Array.isArray(callbackName)
                ? callbackName
                : [callbackName];
            this.wrapperCallbackMap = this.getWrapperCallbackMap(callbackNames);
        }

        getWrapperCallbackMap = (callbackNames) => {
            const wrapperCallbackMap = {};
            for (let i = 0; i < callbackNames.length; i += 1) {
                wrapperCallbackMap[callbackNames[i]] = this.getWrapperCallback(
                    callbackNames[i],
                    metadataName,
                );
            }
            return wrapperCallbackMap;
        };

        getWrapperCallback = (cbName, mdName) => (arg, ...args) => {
            const { [cbName]: callback, [mdName]: data } = this.props;
            if (callback) {
                // TODO: Find a better way to identify a button onPress event.
                if (
                    arg == null ||
                    (arg.nativeEvent &&
                        arg.nativeEvent.changedTouches !== undefined)
                ) {
                    return callback(data);
                }
                const newArgs = [arg];
                if (args) {
                    newArgs.push(...args);
                }
                return callback(...newArgs, data);
            }
            return undefined;
        };

        onRef = (ref) => {
            this.ref = ref;
            // eslint-disable-next-line react/prop-types
            const { childRef } = this.props;
            if (childRef) {
                childRef(ref);
            }
        };

        getWrappedInstance = () => this.ref;

        render() {
            const newProps = { ...this.props };
            _.forOwn(this.wrapperCallbackMap, (wrapperCallback, key) => {
                newProps[key] = wrapperCallback;
            });
            return <PassedComponent {...newProps} ref={this.onRef} />;
        }
    };

export default withDataRelay;
