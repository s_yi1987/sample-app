/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet,
    View,
    ViewPropTypes,
} from 'react-native';
import PropTypes from 'prop-types';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { getCombinedStyles } from '../../api/util';
import Colors from '../../config/Colors';

const FormButton = (props) => {
    const { style, labelStyle, label, leftView, rightView, ...rest } = props;
    const buttonStyle = getCombinedStyles(styles.button, style);
    const newLabelStyle = getCombinedStyles(styles.label, labelStyle);

    return (
        <TouchableOpacity {...rest} style={buttonStyle}>
            <View style={styles.buttonRow}>
                <View style={styles.buttonSideFiller}>{leftView}</View>
                <View style={styles.contentLayout}>
                    <Text style={newLabelStyle}>{label}</Text>
                </View>
                <View style={styles.buttonSideFiller}>{rightView}</View>
            </View>
        </TouchableOpacity>
    );
};

FormButton.propTypes = {
    style: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    labelStyle: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    label: PropTypes.string,
    leftView: PropTypes.node,
    rightView: PropTypes.node,
};

FormButton.defaultProps = {
    style: undefined,
    labelStyle: undefined,
    label: undefined,
    leftView: undefined,
    rightView: undefined,
};

const styles = StyleSheet.create({
    button: {
        backgroundColor: Colors.MAIZE,
        height: hp('8%'),
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonSideFiller: {
        flex: 0.5,
    },
    buttonRow: {
        flexDirection: 'row',
        width: '100%',
        flexGrow: 1,
        alignItems: 'center',
    },
    label: {
        fontSize: hp('3%'),
        color: Colors.BLACK,
    },
    contentLayout: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default FormButton;
