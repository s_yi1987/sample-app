import React, { Component } from 'react';
import { StyleSheet, ViewPropTypes } from 'react-native';
import Animated from 'react-native-reanimated';
import PropTypes from 'prop-types';

export default class Underline extends Component {
    shouldComponentUpdate(nextProps) {
        const { tabMeasurements } = this.props;
        return tabMeasurements !== nextProps.tabMeasurements;
    }

    render() {
        const { style, tabMeasurements, scrollValue } = this.props;

        const { length } = Object.keys(tabMeasurements);
        let inputRange = new Array(length);
        let outputRange = new Array(length);
        let scaleOutputRange = new Array(length);

        for (let i = 0; i < length; i += 1) {
            inputRange[i] = i;
            outputRange[i] =
                tabMeasurements[i].left + tabMeasurements[i].width * 0.5;
            scaleOutputRange[i] = tabMeasurements[i].width;
        }

        // handle case when only one section is present
        if (inputRange.length === 1) {
            inputRange = [-1, ...inputRange];
            outputRange = [0, ...outputRange];
            scaleOutputRange = [0, ...scaleOutputRange];
        }

        const translateX = Animated.multiply(
            Animated.interpolate(scrollValue, {
                inputRange,
                outputRange,
                extrapolate: Animated.Extrapolate.CLAMP,
            }),
            1,
        );
        const scaleX = Animated.interpolate(scrollValue, {
            inputRange,
            outputRange: scaleOutputRange,
            extrapolate: Animated.Extrapolate.CLAMP,
        });
        const additionalStyle = {
            width: 1,
            transform: [{ translateX }, { scaleX }],
        };
        return (
            <Animated.View
                style={[styles.tabUnderlineStyle, style, additionalStyle]}
            />
        );
    }
}

Underline.propTypes = {
    style: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    scrollValue: PropTypes.objectOf(PropTypes.any),
    tabMeasurements: PropTypes.objectOf(PropTypes.any).isRequired,
};

Underline.defaultProps = {
    style: undefined,
    scrollValue: undefined,
};

const styles = StyleSheet.create({
    tabUnderlineStyle: {
        position: 'absolute',
        height: 4,
        backgroundColor: 'navy',
        bottom: 0,
        left: 0,
    },
});
