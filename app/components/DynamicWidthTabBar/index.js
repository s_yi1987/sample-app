import React, { Component } from 'react';
import {
    TouchableOpacity,
    ViewPropTypes,
    View,
    Text,
    StyleSheet,
} from 'react-native';
import { PanGestureHandler, State } from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';
import PropTypes from 'prop-types';

import { getCombinedStyles, compareArrays } from '../../api/util';

import Underline from './Underline';

export default class DynamicWidthTabBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tabContainerWidth: null,
            tabState: null,
            clampedTranslateX: null,
        };

        this.tabState = {};

        this.dragX = new Animated.Value(0);
        this.offsetX = new Animated.Value(0);
        this.velocityX = new Animated.Value(0);
        this.gestureState = new Animated.Value(-1);
        this.decayX = new Animated.Value(0);

        this.decayClock = new Animated.Clock();

        this.onGestureEvent = Animated.event([
            {
                nativeEvent: {
                    translationX: this.dragX,
                    state: this.gestureState,
                    velocityX: this.velocityX,
                },
            },
        ]);
    }

    shouldComponentUpdate(nextProps, nextState) {
        const { scrollValue, tabs } = this.props;
        const { tabState } = this.state;

        if (
            !compareArrays(
                tabs,
                nextProps.tabs,
                (t1, t2) => t1.label === t2.label,
            )
        ) {
            return true;
        }

        return (
            nextProps.scrollValue !== scrollValue ||
            tabState !== nextState.tabState
        );
    }

    getPageScrollOffset = (page, offset = 0) => {
        const { tabMargin } = this.props;
        const containerWidth = this.containerMeasurements.width;
        if (containerWidth >= this.tabContainerMeasurements.width) {
            return 0;
        }

        const tabWidth = this.tabState[page].width;
        const nextTabMeasurements = this.tabState[page + 1];
        const nextTabWidth =
            (nextTabMeasurements && nextTabMeasurements.width) || 0;
        const tabOffset = this.tabState[page].left;

        const absolutePageOffset = offset * (tabWidth + tabMargin);
        let newScrollX = tabOffset + absolutePageOffset;

        newScrollX -=
            (containerWidth - (1 - offset) * tabWidth - offset * nextTabWidth) /
            2;

        const { min, max } = this.getOffsetBoundaries();
        const pageScrollOffset = Math.max(min, Math.min(max, newScrollX));
        return pageScrollOffset;
    };

    getOffsetBoundaries = () => {
        const max =
            this.tabContainerMeasurements.width -
            this.containerMeasurements.width;
        const min = 0;

        return { min, max };
    };

    getPanOffsetBoundaries = () => {
        const { max } = this.getOffsetBoundaries();
        return { max: 0, min: -max };
    };

    renderTab = ({ tab, index, isTabActive, onPress, onLayout }) => {
        const {
            activeTextColor,
            inactiveTextColor,
            textStyle,
            tabStyle,
        } = this.props;
        const textColor = isTabActive ? activeTextColor : inactiveTextColor;
        const fontWeight = isTabActive ? 'bold' : 'normal';

        return (
            <TouchableOpacity
                key={tab.key}
                onPress={() => onPress(index)}
                onLayout={onLayout}
            >
                <View style={[styles.tab, tabStyle]}>
                    <Text style={[{ color: textColor, fontWeight }, textStyle]}>
                        {tab.label}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };

    measureTab = (page, event) => {
        const { x, width, height } = event.nativeEvent.layout;
        if (this.tabState[page]) {
            const {
                left,
                width: prevWidth,
                height: prevHeight,
            } = this.tabState[page];
            if (left === x && prevWidth === width && prevHeight === height) {
                return;
            }
        }
        this.tabState[page] = {
            left: x,
            right: x + width,
            width,
            height,
        };
        this.updateTabStateIfPossible();
    };

    updateTabStateIfPossible = () => {
        if (this.canUpdateTabState()) {
            const translateX = this.getTranslateAnim();

            const { min, max } = this.getPanOffsetBoundaries();
            const minPanOffsetX = Animated.sub(min, translateX);
            const maxPanOffsetX = Animated.multiply(-1, translateX);

            const addX = Animated.add(this.offsetX, this.dragX);
            const panTransX = Animated.cond(
                Animated.eq(this.gestureState, State.BEGAN),
                [
                    Animated.cond(Animated.clockRunning(this.decayClock), [
                        Animated.stopClock(this.decayClock),
                        Animated.set(this.offsetX, this.decayX),
                        Animated.set(this.decayX, 0),
                    ]),
                    Animated.set(
                        this.offsetX,
                        Animated.min(
                            Animated.max(this.offsetX, minPanOffsetX),
                            maxPanOffsetX,
                        ),
                    ),
                    addX,
                ],
                Animated.cond(
                    Animated.eq(this.gestureState, State.ACTIVE),
                    addX,
                    [
                        Animated.set(this.offsetX, addX),
                        Animated.cond(
                            Animated.eq(this.velocityX, 0),
                            this.offsetX,
                            Animated.set(this.decayX, this.runDecay()),
                        ),
                    ],
                ),
            );

            const addAnim = Animated.add(translateX, panTransX);
            const clampedTranslateX = Animated.interpolate(addAnim, {
                inputRange: [min, max],
                outputRange: [min, max],
                extrapolate: Animated.Extrapolate.CLAMP,
            });

            const { width: tabContainerWidth } = this.tabContainerMeasurements;
            const { width: containerWidth } = this.containerMeasurements;
            const width = Math.max(tabContainerWidth, containerWidth);

            this.setState({
                tabState: { ...this.tabState },
                clampedTranslateX,
                tabContainerWidth: width,
            });
        }
    };

    runDecay = () => {
        const state = {
            finished: new Animated.Value(0),
            position: new Animated.Value(0),
            velocity: new Animated.Value(0),
            time: new Animated.Value(0),
        };
        const { decelerationRate } = this.props;
        const config = { deceleration: decelerationRate };
        return Animated.block([
            Animated.cond(Animated.clockRunning(this.decayClock), 0, [
                Animated.set(state.finished, 0),
                Animated.set(state.time, 0),
                Animated.set(state.position, this.offsetX),
                Animated.set(
                    state.velocity,
                    Animated.divide(this.velocityX),
                    1000,
                ),
                Animated.startClock(this.decayClock),
            ]),
            Animated.cond(
                Animated.clockRunning(this.decayClock),
                Animated.decay(this.decayClock, state, config),
            ),
            Animated.cond(state.finished, [
                Animated.stopClock(this.decayClock),
                Animated.set(this.offsetX, state.position),
            ]),
            state.position,
        ]);
    };

    getTranslateAnim = () => {
        const { scrollValue } = this.props;
        const { length } = Object.keys(this.tabState);

        const inputRange = new Array(length);
        const outputRange = new Array(length);

        for (let i = 0; i < length; i += 1) {
            inputRange[i] = i;
            outputRange[i] = -this.getPageScrollOffset(i);
        }

        return Animated.interpolate(scrollValue, {
            inputRange,
            outputRange,
            extrapolate: Animated.Extrapolate.CLAMP,
        });
    };

    canUpdateTabState = () => {
        const { tabs } = this.props;
        return (
            tabs.length === Object.keys(this.tabState).length &&
            this.tabContainerMeasurements &&
            this.containerMeasurements
        );
    };

    renderUnderline = () => {
        const { tabState } = this.state;
        const { underlineStyle, scrollValue, tabs } = this.props;

        if (!tabState || tabs.length < 1) {
            return null;
        }

        return (
            <Underline
                style={underlineStyle}
                tabMeasurements={tabState}
                scrollValue={scrollValue}
            />
        );
    };

    onTabContainerLayout = (e) => {
        const { layout } = e.nativeEvent;
        if (this.tabContainerMeasurements) {
            const {
                width: prevWidth,
                height: prevHeight,
            } = this.tabContainerMeasurements;
            if (layout.width === prevWidth && layout.height === prevHeight) {
                return;
            }
        }
        this.tabContainerMeasurements = layout;
        this.updateTabStateIfPossible();
    };

    onContainerLayout = (e) => {
        const { layout } = e.nativeEvent;
        if (this.containerMeasurements) {
            const {
                width: prevWidth,
                height: prevHeight,
            } = this.containerMeasurements;
            if (layout.width === prevWidth && layout.height === prevHeight) {
                return;
            }
        }
        this.containerMeasurements = layout;
        this.updateTabStateIfPossible();
    };

    getGestureViewOffsetStyle = () => {
        const { clampedTranslateX } = this.state;
        if (!clampedTranslateX) {
            return {};
        }
        return {
            position: 'absolute',
            transform: [{ translateX: clampedTranslateX }],
        };
    };

    render() {
        const {
            style,
            tabs,
            tabsContainerStyle,
            renderTab,
            goToPage,
            activeTab,
            scrollX,
        } = this.props;

        const { tabContainerWidth, clampedTranslateX } = this.state;
        let panGestureEnabled =
            clampedTranslateX != null &&
            tabContainerWidth != null &&
            tabs.length > 0;

        if (panGestureEnabled) {
            const containerWidth = this.containerMeasurements.width;
            const actualTabContainerWidth = this.tabContainerMeasurements.width;
            if (actualTabContainerWidth <= containerWidth) {
                panGestureEnabled = false;
            }
        }

        return (
            <View
                style={getCombinedStyles(styles.container, style)}
                onLayout={this.onContainerLayout}
            >
                <Animated.Code
                    exec={Animated.onChange(scrollX, [
                        Animated.cond(
                            Animated.and(
                                Animated.defined(this.decayClock),
                                Animated.clockRunning(this.decayClock),
                            ),
                            [
                                Animated.stopClock(this.decayClock),
                                Animated.set(this.velocityX, 0),
                                Animated.set(this.decayX, 0),
                            ],
                        ),
                        Animated.set(this.offsetX, 0),
                        Animated.set(this.dragX, 0),
                    ])}
                />
                <PanGestureHandler
                    enabled={panGestureEnabled}
                    shouldCancelWhenOutside={false}
                    maxPointers={1}
                    onGestureEvent={this.onGestureEvent}
                    onHandlerStateChange={this.onGestureEvent}
                    activeOffsetX={[-4, 4]}
                >
                    <Animated.View
                        style={[
                            styles.gestureContainer,
                            {
                                width: tabContainerWidth,
                                ...this.getGestureViewOffsetStyle(),
                            },
                        ]}
                    >
                        <View
                            style={[
                                styles.tabs,
                                { width: tabContainerWidth },
                                tabsContainerStyle,
                            ]}
                            onLayout={this.onTabContainerLayout}
                        >
                            {tabs.map((tab, page) => {
                                const isTabActive = activeTab === page;
                                const renderTabFunc =
                                    renderTab || this.renderTab;
                                return renderTabFunc({
                                    tab,
                                    index: page,
                                    isTabActive,
                                    onPress: goToPage,
                                    onLayout: this.measureTab.bind(this, page),
                                });
                            })}
                            {this.renderUnderline()}
                        </View>
                    </Animated.View>
                </PanGestureHandler>
            </View>
        );
    }
}

DynamicWidthTabBar.propTypes = {
    goToPage: PropTypes.func,
    activeTab: PropTypes.number.isRequired,
    tabs: PropTypes.array.isRequired,
    activeTextColor: PropTypes.string,
    inactiveTextColor: PropTypes.string,
    style: ViewPropTypes.style,
    tabStyle: ViewPropTypes.style,
    tabsContainerStyle: ViewPropTypes.style,
    textStyle: ViewPropTypes.style,
    renderTab: PropTypes.func,
    underlineStyle: ViewPropTypes.style,
    scrollValue: PropTypes.object,
    scrollX: PropTypes.instanceOf(Animated.Value),
    tabMargin: PropTypes.number,
    decelerationRate: PropTypes.number,
};

DynamicWidthTabBar.defaultProps = {
    goToPage: () => {},
    activeTextColor: 'navy',
    inactiveTextColor: 'black',
    style: {},
    tabStyle: {},
    textStyle: {},
    renderTab: undefined,
    tabsContainerStyle: {},
    underlineStyle: {},
    scrollValue: undefined,
    scrollX: undefined,
    tabMargin: 0,
    decelerationRate: 0.99,
};

const styles = StyleSheet.create({
    tab: {
        // height: wsv(39),
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 20,
        paddingRight: 20,
    },
    container: {
        width: '100%',
        borderWidth: 1,
        borderTopWidth: 0,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        borderColor: '#ccc',
        backgroundColor: 'white',
        overflow: 'hidden',
    },
    gestureContainer: {
        position: 'absolute',
        left: 0,
        height: '100%',
        overflow: 'hidden',
    },
    tabs: {
        flexDirection: 'row',
    },
});
