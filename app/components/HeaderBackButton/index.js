import React from 'react';
import { HeaderBackButton as RNHeaderBackButton } from '@react-navigation/stack';

const HeaderBackButton = (props) => <RNHeaderBackButton {...props} />;

export default HeaderBackButton;
