import React, { PureComponent } from 'react';
import { View, FlatList, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';
import memoizeOne from 'memoize-one';
import Animated from 'react-native-reanimated';

import LinkedScrollNode from './LinkedScrollNode';

const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);

const createDefaultOnScrollHandler = ({ animatedOffsetX }) => {
    return Animated.event([
        { nativeEvent: { contentOffset: { x: animatedOffsetX } } },
    ]);
};

export default class TabView extends PureComponent {
    constructor(props) {
        super(props);
        const { initialPage, pageDimensions, createOnScrollHandler } = props;

        const containerWidth = pageDimensions ? pageDimensions.width : null;
        const {
            scrollX = new Animated.Value(initialPage * (containerWidth || 1)),
        } = props;

        const wrappedScrollX = Animated.block([
            Animated.call([scrollX], this.onScroll),
            scrollX,
        ]);

        const containerWidthAnimatedValue = new Animated.Value(containerWidth);
        const scrollValue = Animated.divide(
            wrappedScrollX,
            containerWidthAnimatedValue,
        );

        this.state = {
            currentPage: initialPage,
            scrollValue,
            scrollX,
            containerWidth,
            containerHeight: pageDimensions ? pageDimensions.height : null,
        };

        this.scrollOnMountCalled = false;
        this.scrollViewRef = React.createRef();
        this.onScrollEventHandler = createOnScrollHandler({
            animatedOffsetX: scrollX,
            onScrollX: this.onScroll,
        });
        this.viewabilityConfig = {
            minimumViewTime: 200,
            viewAreaCoveragePercentThreshold: 70,
        };
    }

    componentDidUpdate() {
        const { page } = this.props;
        const { currentPage } = this.state;
        if (page >= 0 && page !== currentPage) {
            this.goToPage(page);
        }
    }

    getExtraState = memoizeOne((extraData, width, height) => ({
        ...extraData,
        containerWidth: width,
        containerHeight: height,
    }));

    renderScrollableContent = () => {
        const {
            contentProps,
            scrollEnabled,
            initialPage,
            extraData,
            data,
        } = this.props;

        const { containerWidth, containerHeight } = this.state;

        if (containerWidth == null) {
            return null;
        }
        const initialScrollIndex = initialPage <= 0 ? undefined : initialPage;

        return (
            <AnimatedFlatList
                horizontal
                pagingEnabled
                data={data}
                onLayout={this.handleLayout}
                renderItem={this.renderItem}
                automaticallyAdjustContentInsets={false}
                ref={this.scrollViewRef}
                onScroll={this.onScrollEventHandler}
                scrollEventThrottle={16}
                scrollsToTop={false}
                showsHorizontalScrollIndicator={false}
                scrollEnabled={scrollEnabled}
                directionalLockEnabled
                alwaysBounceVertical={false}
                keyboardDismissMode="on-drag"
                bounces={false}
                overScrollMode="never"
                alwaysBounceHorizontal={false}
                keyboardShouldPersistTaps="handled"
                getItemLayout={this.getItemLayout}
                initialScrollIndex={initialScrollIndex}
                viewabilityConfig={this.viewabilityConfig}
                onViewableItemsChanged={this.onViewableItemsChanged}
                extraData={this.getExtraState(
                    extraData,
                    containerWidth,
                    containerHeight,
                )}
                {...contentProps}
            />
        );
    };

    onViewableItemsChanged = ({ viewableItems }) => {
        if (viewableItems.length < 1) {
            return;
        }
        const { index } = viewableItems[0];
        const { onViewableTabChanged } = this.props;
        if (onViewableTabChanged) {
            onViewableTabChanged(index);
        }
    };

    getItemLayout = (_data, index) => {
        const { containerWidth } = this.state;
        return {
            length: containerWidth,
            offset: containerWidth * index,
            index,
        };
    };

    updateSelectedPage = (nextPage) => {
        const { currentPage } = this.state;

        this.updateSceneKeys({
            page: nextPage,
            preCallback: () => {
                const { onWillChangeTab } = this.props;
                onWillChangeTab(nextPage, currentPage);
            },
            callback: () => {
                this.onChangeTab(currentPage, nextPage);
            },
        });
    };

    onChangeTab = (prevPage, currentPage) => {
        const { onChangeTab } = this.props;
        onChangeTab(currentPage, prevPage);
    };

    onScroll = ([offsetX]) => {
        const { containerWidth, currentPage } = this.state;
        const newScrollValue = offsetX / containerWidth;

        if (offsetX === 0 && !this.scrollOnMountCalled) {
            this.scrollOnMountCalled = true;
        } else {
            const { onScroll } = this.props;
            onScroll(newScrollValue);
        }

        const page = Math.round(newScrollValue);
        if (currentPage !== page) {
            this.updateSelectedPage(page);
        }
    };

    createTabBarProps = () => {
        const { scrollX } = this.state;
        const {
            tabBarPosition,
            data,
            tabBarBackgroundColor,
            tabBarActiveTextColor,
            tabBarInactiveTextColor,
            tabBarTextStyle,
            tabBarUnderlineStyle,
        } = this.props;
        const { currentPage, scrollValue, containerWidth } = this.state;
        const overlayTabs =
            tabBarPosition === 'overlayTop' ||
            tabBarPosition === 'overlayBottom';
        const tabBarProps = {
            goToPage: this.goToPage,
            tabs: data.map((item) => item.tabLabel),
            activeTab: currentPage,
            scrollValue,
            containerWidth,
            scrollX,
        };

        if (tabBarBackgroundColor) {
            tabBarProps.backgroundColor = tabBarBackgroundColor;
        }
        if (tabBarActiveTextColor) {
            tabBarProps.activeTextColor = tabBarActiveTextColor;
        }
        if (tabBarInactiveTextColor) {
            tabBarProps.inactiveTextColor = tabBarInactiveTextColor;
        }
        if (tabBarTextStyle) {
            tabBarProps.textStyle = tabBarTextStyle;
        }
        if (tabBarUnderlineStyle) {
            tabBarProps.underlineStyle = tabBarUnderlineStyle;
        }
        if (overlayTabs) {
            tabBarProps.style = {
                position: 'absolute',
                left: 0,
                right: 0,
                [tabBarPosition === 'overlayTop' ? 'top' : 'bottom']: 0,
            };
        }
        return tabBarProps;
    };

    updateSceneKeys = ({
        page,
        callback = () => {},
        preCallback = () => {},
    }) => {
        preCallback();
        this.setState({ currentPage: page }, callback);
    };

    goToPage = (pageNumber) => {
        this.scrollViewRef.current.getNode().scrollToIndex({
            index: pageNumber,
            viewOffset: 0,
            animated: true,
        });
    };

    handleLayout = (e) => {
        const { height } = e.nativeEvent.layout;
        const { onPagerLayout } = this.props;
        const { containerHeight } = this.state;

        if (Math.round(height) !== Math.round(containerHeight)) {
            this.setState({ containerHeight: height });
        }
        if (onPagerLayout) {
            onPagerLayout(e);
        }
    };

    renderItem = ({ item, index }) => {
        const { containerWidth, containerHeight } = this.state;
        const { renderItem } = this.props;
        return (
            <View
                style={{
                    width: containerWidth,
                    height: containerHeight,
                }}
            >
                {renderItem(item, index)}
            </View>
        );
    };

    renderTabBar = (tabBarProps) => {
        const { renderTabBar } = this.props;
        if (renderTabBar) {
            return renderTabBar(tabBarProps);
        }
        return null;
    };

    onContainerLayout = (e) => {
        const { width } = e.nativeEvent.layout;
        const { onLayout } = this.props;
        const { containerWidth, scrollX, currentPage } = this.state;

        if (Math.round(width) !== Math.round(containerWidth)) {
            const containerWidthAnimatedValue = new Animated.Value(width);
            const scrollValue = Animated.divide(
                scrollX,
                containerWidthAnimatedValue,
            );

            this.setState({
                containerWidth: width,
                scrollValue,
            });
            this.goToPage(currentPage);
        }
        if (onLayout) {
            onLayout(e);
        }
    };

    render() {
        const { style, tabBarPosition, linkedScrollValue } = this.props;
        const { scrollValue } = this.state;
        const overlayTabs =
            tabBarPosition === 'overlayTop' ||
            tabBarPosition === 'overlayBottom';
        const tabBarProps = this.createTabBarProps();

        return (
            <View onLayout={this.onContainerLayout} style={style}>
                <LinkedScrollNode
                    scrollValue={scrollValue}
                    linkedScrollValue={linkedScrollValue}
                />
                {tabBarPosition === 'top' && this.renderTabBar(tabBarProps)}
                {this.renderScrollableContent()}
                {(tabBarPosition === 'bottom' || overlayTabs) &&
                    this.renderTabBar(tabBarProps)}
            </View>
        );
    }
}

TabView.propTypes = {
    style: PropTypes.oneOfType([ViewPropTypes.style, PropTypes.array]),
    tabBarPosition: PropTypes.oneOf([
        'top',
        'bottom',
        'overlayTop',
        'overlayBottom',
    ]),
    initialPage: PropTypes.number,
    page: PropTypes.number,
    pageDimensions: PropTypes.shape({
        width: PropTypes.number,
        height: PropTypes.number,
    }),
    onChangeTab: PropTypes.func,
    onWillChangeTab: PropTypes.func,
    onScroll: PropTypes.func,
    renderTabBar: PropTypes.func,
    contentProps: PropTypes.object,
    scrollEnabled: PropTypes.bool,
    extraData: PropTypes.object,
    renderItem: PropTypes.func.isRequired,
    onLayout: PropTypes.func,
    onPagerLayout: PropTypes.func,
    onViewableTabChanged: PropTypes.func,
    data: PropTypes.array.isRequired,
    tabBarBackgroundColor: PropTypes.string,
    tabBarActiveTextColor: PropTypes.string,
    tabBarInactiveTextColor: PropTypes.string,
    tabBarTextStyle: PropTypes.string,
    tabBarUnderlineStyle: PropTypes.string,
    scrollX: PropTypes.instanceOf(Animated.Value),
    linkedScrollValue: PropTypes.object,
    createOnScrollHandler: PropTypes.func,
};

TabView.defaultProps = {
    style: undefined,
    tabBarPosition: 'top',
    initialPage: 0,
    page: -1,
    pageDimensions: undefined,
    onChangeTab: () => {},
    onWillChangeTab: () => {},
    onScroll: () => {},
    renderTabBar: undefined,
    contentProps: {},
    scrollEnabled: true,
    extraData: undefined,
    onLayout: undefined,
    onPagerLayout: undefined,
    onViewableTabChanged: undefined,
    tabBarBackgroundColor: undefined,
    tabBarActiveTextColor: undefined,
    tabBarInactiveTextColor: undefined,
    tabBarTextStyle: undefined,
    tabBarUnderlineStyle: undefined,
    scrollX: undefined,
    linkedScrollValue: undefined,
    createOnScrollHandler: createDefaultOnScrollHandler,
};
