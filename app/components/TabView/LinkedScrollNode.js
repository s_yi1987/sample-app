import React from 'react';
import Animated from 'react-native-reanimated';
import PropTypes from 'prop-types';

const LinkedScrollNode = React.memo((props) => {
    const { linkedScrollValue, scrollValue } = props;
    if (!linkedScrollValue || !scrollValue) {
        return null;
    }

    return (
        <Animated.Code
            exec={Animated.onChange(scrollValue, [
                Animated.set(linkedScrollValue, scrollValue),
            ])}
        />
    );
});

LinkedScrollNode.propTypes = {
    linkedScrollValue: PropTypes.object,
    scrollValue: PropTypes.object.isRequired,
};

LinkedScrollNode.defaultProps = {
    linkedScrollValue: undefined,
};

export default LinkedScrollNode;
