import React from 'react';
import { Image, View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import PropTypes from 'prop-types';

import Colors from '../../config/Colors';

export const calculateCellHeight = () => hp('13%');

const ItemListCell = React.memo((props) => {
    const { source, label, subLabel, onPress } = props;
    return (
        <TouchableOpacity style={styles.cell} onPress={onPress}>
            <View style={styles.imageContainer}>
                <Image
                    style={styles.image}
                    source={source}
                    resizeMode="cover"
                />
            </View>
            <View style={styles.textContainer}>
                <Text style={styles.labelText}>{label}</Text>
                <Text style={styles.subLabelText}>{subLabel}</Text>
            </View>
        </TouchableOpacity>
    );
});

ItemListCell.propTypes = {
    source: PropTypes.oneOfType([
        PropTypes.shape({
            uri: PropTypes.string,
            headers: PropTypes.objectOf(PropTypes.string),
        }),
        PropTypes.number,
        PropTypes.arrayOf(
            PropTypes.shape({
                uri: PropTypes.string,
                width: PropTypes.number,
                height: PropTypes.number,
                headers: PropTypes.objectOf(PropTypes.string),
            }),
        ),
    ]),
    label: PropTypes.string,
    subLabel: PropTypes.string,
    onPress: PropTypes.func,
};

ItemListCell.defaultProps = {
    source: undefined,
    label: undefined,
    subLabel: undefined,
    onPress: undefined,
};

const styles = StyleSheet.create({
    cell: {
        height: calculateCellHeight(),
        flex: 1,
        flexDirection: 'row',
    },
    imageContainer: {
        height: '100%',
        marginLeft: wp('1%'),
        marginRight: wp('3%'),
        justifyContent: 'center',
    },
    image: {
        height: '95%',
        aspectRatio: 1,
    },
    textContainer: {
        flex: 1,
    },
    labelText: {
        fontSize: wp('4%'),
        color: Colors.BLACK,
    },
    subLabelText: {
        marginTop: wp('2%'),
        fontSize: wp('3%'),
        color: Colors.COOL_GREY,
    },
});

export default ItemListCell;
