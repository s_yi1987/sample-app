import React from 'react';
import { View, FlatList, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import ItemListCell, { calculateCellHeight } from './ItemListCell';
import Colors from '../../config/Colors';

const renderItem = ({ item }) => <ItemListCell {...item} />;

const getItemLayout = (data, index) => ({
    length: calculateCellHeight(data) + styles.separator.height,
    offset: (calculateCellHeight(data) + styles.separator.height) * index,
    index,
});

const renderSeparator = () => <View style={styles.separator} />;

const ItemList = (props) => (
    <FlatList
        {...props}
        renderItem={renderItem}
        getItemLayout={getItemLayout}
        ItemSeparatorComponent={renderSeparator}
    />
);

ItemList.propTypes = {
    windowSize: PropTypes.number,
    initialNumToRender: PropTypes.number,
    showsVerticalScrollIndicator: PropTypes.bool,
};

ItemList.defaultProps = {
    windowSize: 11,
    initialNumToRender: 7,
    showsVerticalScrollIndicator: false,
};

const styles = StyleSheet.create({
    separator: {
        height: 1,
        flex: 1,
        backgroundColor: Colors.LIGHT_GREY,
    },
});

export default ItemList;
