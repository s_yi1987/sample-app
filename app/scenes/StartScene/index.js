import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';

const StartScene = (props) => {
    const { navigation, route } = props;
    useEffect(() => {
        navigation.setParams({ proceedToApp: true });
        navigation.navigate('MapScene', {
            preventBack: true,
            nextScene: 'StartScene',
        });
    }, []);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            if (route.params && route.params.proceedToApp) {
                navigation.setParams({ proceedToApp: undefined });

                navigation.navigate('AppMainScene');
            }
        });

        return unsubscribe;
    }, [navigation, route.params]);

    return <View style={styles.root} />;
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
});

export default StartScene;
