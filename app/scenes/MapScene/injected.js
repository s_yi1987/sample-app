import { connect } from 'react-redux';

import { setLocation } from '../../store/actions/location';
import { setBranch } from '../../store/actions/branch';

import MapScene from './index';

const mapDispatchToProps = (dispatch) => ({
    setLocation: (location) => {
        dispatch(setLocation(location));
    },
    setBranch: (branch) => {
        dispatch(setBranch(branch));
    },
});

const ReduxAwareMapScene = connect(null, mapDispatchToProps)(MapScene);

export default ReduxAwareMapScene;
