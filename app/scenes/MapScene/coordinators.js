import React, { useCallback } from 'react';
import hideBackButtonIfNeeded from '../../hooks/hideBackButtonIfNeeded';

export default (MapScene) => (props) => {
    const { navigation, route, ...rest } = props;

    const onBranchSet = useCallback(
        ({ selectedBranch }) => {
            navigation.navigate(route.params.nextScene, {
                selectedBranch,
            });
        },
        [navigation, route],
    );

    hideBackButtonIfNeeded({ route, navigation });

    return <MapScene {...rest} onBranchSet={onBranchSet} />;
};
