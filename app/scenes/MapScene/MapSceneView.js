import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import MapView, { PROVIDER_DEFAULT } from 'react-native-maps';
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import FormButton from '../../components/FormButton';

import iconMapMarker from '../../assets/images/iconPinBlackBig/iconPinBlackBig.png';

const MapSceneView = (props) => {
    const { initialRegion, onButtonPress, onRegionChangeComplete } = props;
    return (
        <View style={styles.root}>
            <View style={styles.topContainer}>
                <MapView
                    style={styles.root}
                    provider={PROVIDER_DEFAULT}
                    initialRegion={initialRegion}
                    onRegionChangeComplete={onRegionChangeComplete}
                />
                <View style={styles.overlayView} pointerEvents="none">
                    <Image
                        style={styles.markerIcon}
                        source={iconMapMarker}
                        resizeMode="contain"
                    />
                </View>
            </View>
            <View style={styles.bottomContainer}>
                <View style={styles.root} />
                <FormButton
                    label="Select"
                    style={styles.button}
                    onPress={onButtonPress}
                />
            </View>
        </View>
    );
};

export default MapSceneView;

MapSceneView.propTypes = {
    initialRegion: PropTypes.shape({
        latitude: PropTypes.number,
        longitude: PropTypes.number,
        latitudeDelta: PropTypes.number,
        longitudeDelta: PropTypes.number,
    }),
    onButtonPress: PropTypes.func,
    onRegionChangeComplete: PropTypes.func,
};

MapSceneView.defaultProps = {
    initialRegion: undefined,
    onButtonPress: () => {},
    onRegionChangeComplete: () => {},
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
    topContainer: {
        flex: 0.8,
    },
    bottomContainer: {
        flex: 0.2,
    },
    button: {
        marginBottom: hp('3%'),
        marginHorizontal: wp('5%'),
    },
    markerIcon: {
        width: wp('10%'),
        height: wp('10%'),
    },
    overlayView: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
});
