import React, { useContext } from 'react';

import ApiLayerContext from '../../contexts/ApiLayerContext';

export default (MapScene) => (props) => {
    const api = useContext(ApiLayerContext);

    return (
        <MapScene
            {...props}
            fetchNearestBranch={api.fetchNearestBranch}
            getCurrentPosition={api.getCurrentPosition}
        />
    );
};
