import React, { useEffect, useCallback, useRef, useState } from 'react';
import {
    View,
    ActivityIndicator,
    SafeAreaView,
    StyleSheet,
} from 'react-native';
import _ from 'lodash';
import PropTypes from 'prop-types';

import MapSceneView from './MapSceneView';
import { getRegion } from '../../api/util';

const renderPlaceholder = () => (
    <View style={styles.placeholder}>
        <ActivityIndicator size="large" />
    </View>
);

const MapScene = (props) => {
    const {
        setLocation,
        setBranch,
        fetchNearestBranch,
        getCurrentPosition,
        onBranchSet,
        debounceTime,
    } = props;

    const [initialRegion, setInitialRegion] = useState();
    const currentRegion = useRef();

    const onRegionChangeComplete = useCallback(
        (region) => {
            currentRegion.current = region;
        },
        [currentRegion],
    );

    const onButtonPress = useCallback(
        _.debounce(
            async () => {
                try {
                    const nearestBranch = await fetchNearestBranch(
                        currentRegion.current,
                    );
                    setLocation({
                        longitude: currentRegion.current.longitude,
                        latitude: currentRegion.current.latitude,
                    });
                    setBranch(nearestBranch);
                    onBranchSet({ selectedBranch: nearestBranch });
                } catch (e) {
                    // TODO: Could use a Toast to display the error message
                    console.log('Error occured onButtonPress:', e);
                }
            },
            debounceTime,
            {
                leading: true,
                trailing: false,
            },
        ),
        [fetchNearestBranch, onBranchSet, debounceTime],
    );

    useEffect(() => {
        const setupInitialRegion = async () => {
            const position = await getCurrentPosition();
            const region = getRegion(
                position.longitude,
                position.latitude,
                2000000,
            );
            currentRegion.current = region;
            setInitialRegion(region);
        };

        setupInitialRegion();
    }, []);

    return (
        <SafeAreaView style={styles.root}>
            {initialRegion ? (
                <MapSceneView
                    initialRegion={initialRegion}
                    onButtonPress={onButtonPress}
                    onRegionChangeComplete={onRegionChangeComplete}
                />
            ) : (
                renderPlaceholder()
            )}
        </SafeAreaView>
    );
};

MapScene.propTypes = {
    setLocation: PropTypes.func,
    setBranch: PropTypes.func,
    fetchNearestBranch: PropTypes.func.isRequired,
    getCurrentPosition: PropTypes.func.isRequired,
    onBranchSet: PropTypes.func,
    debounceTime: PropTypes.number,
};

MapScene.defaultProps = {
    setLocation: () => {},
    setBranch: () => {},
    onBranchSet: () => {},
    debounceTime: 2000,
};

const styles = StyleSheet.create({
    root: {
        flex: 1,
        backgroundColor: 'white',
    },
    placeholder: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default MapScene;
