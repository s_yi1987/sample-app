import React, { useMemo, useEffect, useState, useCallback } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import PropTypes from 'prop-types';

import RestaurantTabView from './RestaurantTabView';

export const generatePages = ({ branch }) =>
    branch.foodCategories.map((foodCategory) => ({
        tabLabel: {
            key: `${foodCategory}Tab`,
            label: foodCategory,
        },
        key: `${foodCategory}Page`,
        items: [],
    }));

export const appendPage = ({ index, data, items }) => {
    const newData = data.slice(0);
    newData[index].items = newData[index].items.slice(0);
    newData[index].items.push(...items);

    return newData;
};

const RestaurantScene = (props) => {
    const {
        branch,
        location,
        fetchCategoryData,
        contentProps,
        testID,
        pageDimensions,
        ...rest
    } = props;
    const [data, setData] = useState([]);
    const [branchKey, setBranchKey] = useState(0);

    useEffect(() => {
        if (!branch || !branch.foodCategories) {
            return;
        }
        setData(generatePages({ branch }));
        setBranchKey(branchKey + 1);
    }, [branch, fetchCategoryData]);

    useEffect(() => {
        // TODO: Track index to loadMore in current tab
        loadMore(0);
    }, [branchKey]);

    const tabViewPageWidth =
        pageDimensions.width || Dimensions.get('window').width;
    const tabViewPageHeight =
        pageDimensions.height || Dimensions.get('window').height;

    const tabPageDimensions = useMemo(
        () => ({
            width: tabViewPageWidth,
            height: tabViewPageHeight,
        }),
        [tabViewPageWidth, tabViewPageHeight],
    );

    // TODO: This should be modified dynamically based on the memory
    // of the device
    const tabViewContentProps = useMemo(
        () => ({
            initialNumToRender: 1,
            updateCellsBatchingPeriod: 90,
            maxToRenderPerBatch: 2,
            windowSize: 5,
            ...contentProps,
        }),
        [],
    );

    const loadMore = useCallback(
        async (index) => {
            if (data[index] && data[index].items.length < 1) {
                const listItemProps = await fetchCategoryData({
                    branch,
                    index,
                    location,
                });
                setData((currentData) =>
                    appendPage({
                        index,
                        data: currentData,
                        items: listItemProps,
                    }),
                );
            }
        },
        [data, fetchCategoryData, location, branch],
    );

    if (data.length < 1) {
        return null;
    }
    return (
        <View testID={testID} style={styles.root}>
            <RestaurantTabView
                {...rest}
                key={`branch${branchKey}user`}
                style={styles.root}
                data={data}
                onChangeTab={loadMore}
                contentProps={tabViewContentProps}
                pageDimensions={tabPageDimensions}
            />
        </View>
    );
};

RestaurantScene.propTypes = {
    branch: PropTypes.object,
    location: PropTypes.object,
    fetchCategoryData: PropTypes.func,
    contentProps: PropTypes.object,
    testID: PropTypes.string,
    pageDimensions: PropTypes.shape({
        width: PropTypes.number,
        height: PropTypes.number,
    }),
};

RestaurantScene.defaultProps = {
    branch: undefined,
    location: undefined,
    fetchCategoryData: () => Promise.resolve([]),
    contentProps: {},
    testID: undefined,
    pageDimensions: {},
};

const styles = StyleSheet.create({
    root: { flex: 1, backgroundColor: 'white' },
});

export default RestaurantScene;
