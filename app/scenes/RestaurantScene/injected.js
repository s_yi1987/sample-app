import { connect } from 'react-redux';

import RestaurantScene from './index';

const mapStateToProps = (state) => ({
    branch: state.branch.branch,
    location: state.location.location,
});

const ReduxAwareRestaurantScene = connect(mapStateToProps)(RestaurantScene);

export default ReduxAwareRestaurantScene;
