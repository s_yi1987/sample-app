import React, { useContext } from 'react';
import convert from 'convert-units';

import ApiLayerContext from '../../contexts/ApiLayerContext';
import { getDistanceFromLngLat } from '../../api/util';

export default (RestaurantScene) => (props) => {
    const api = useContext(ApiLayerContext);

    const fetchCategoryData = async ({ index, branch, location }) => {
        const pageItems = await api.fetchRestaurants({
            branchId: branch.id,
            category: branch.foodCategories[index],
        });

        return pageItems.map((restaurant) => {
            const { val, unit } = convert(
                getDistanceFromLngLat(location, {
                    longitude: restaurant.longitude,
                    latitude: restaurant.latitude,
                }),
            )
                .from('km')
                .toBest({ exclude: ['cm', 'mm'] });
            const subLabel = `${Math.round(val)} ${unit}`;
            return {
                label: restaurant.name,
                source: restaurant.image,
                subLabel,
                key: restaurant.name + branch.foodCategories[index],
            };
        });
    };

    return <RestaurantScene {...props} fetchCategoryData={fetchCategoryData} />;
};
