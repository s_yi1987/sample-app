import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

import TabView from '../../components/TabView';
import AnimatedTextTabBar from '../../components/AnimatedTextTabBar';
import ItemList from '../../components/ItemList';

import Colors from '../../config/Colors';

export const renderPage = (page) => {
    if (page.items.length < 1) {
        return (
            <View style={styles.placeholder}>
                <ActivityIndicator
                    size="large"
                    animating
                    color={Colors.GOLDEN_ROD}
                />
            </View>
        );
    }
    return <ItemList style={styles.list} data={page.items} />;
};

export const renderTabBar = (props) => {
    return <AnimatedTextTabBar {...props} />;
};

const RestaurantTabView = (props) => {
    return (
        <TabView
            {...props}
            tabBarPosition="top"
            renderItem={renderPage}
            renderTabBar={renderTabBar}
        />
    );
};

const styles = StyleSheet.create({
    placeholder: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
    },
    list: {
        flex: 1,
        backgroundColor: 'white',
        marginTop: hp('2%'),
    },
});

export default RestaurantTabView;
