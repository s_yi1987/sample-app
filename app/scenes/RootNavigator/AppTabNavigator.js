import React, { useMemo, useContext } from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import SceneFactoryContext from '../../contexts/SceneFactoryContext';

import FavRestaurantScene from '../FavRestaurantScene';
import MyPageScene from '../MyPageScene';

import Colors from '../../config/Colors';

import iconHome from '../../assets/images/iconHome/iconHome.png';
import iconFavorite from '../../assets/images/iconFavorite/iconFavorite.png';
import iconMyPage from '../../assets/images/iconMyPage/iconMyPage.png';

const AppTab = createBottomTabNavigator();
const RestaurantTabStack = createStackNavigator();

const AppTabNavigator = () => {
    const factory = useContext(SceneFactoryContext);

    const RestaurantTabScreen = useMemo(() => {
        return () => (
            <RestaurantTabStack.Navigator initialRouteName="RestaurantScene">
                <RestaurantTabStack.Screen
                    name="RestaurantScene"
                    component={factory.restaurantScene()}
                    options={{ title: 'Restaurants' }}
                />
            </RestaurantTabStack.Navigator>
        );
    }, [factory]);

    return (
        <AppTab.Navigator
            initialRouteName="RestaurantTabScene"
            tabBarOptions={{
                activeTintColor: Colors.GOLDEN_ROD,
            }}
        >
            <AppTab.Screen
                name="RestaurantTabScene"
                component={RestaurantTabScreen}
                options={{
                    title: 'Restaurants',
                    tabBarLabel: 'Restaurants',
                    // eslint-disable-next-line react/prop-types
                    tabBarIcon: ({ color, size }) => (
                        <Image
                            source={iconHome}
                            style={{
                                width: size,
                                height: size,
                                tintColor: color,
                            }}
                        />
                    ),
                }}
            />
            <AppTab.Screen
                name="FavoriteScene"
                component={FavRestaurantScene}
                options={{
                    tabBarLabel: 'Favorites',
                    // eslint-disable-next-line react/prop-types
                    tabBarIcon: ({ color, size }) => (
                        <Image
                            source={iconFavorite}
                            style={{
                                width: size,
                                height: size,
                                tintColor: color,
                            }}
                        />
                    ),
                }}
            />
            <AppTab.Screen
                name="MyPageScene"
                component={MyPageScene}
                options={{
                    tabBarLabel: 'My Page',
                    // eslint-disable-next-line react/prop-types
                    tabBarIcon: ({ color, size }) => (
                        <Image
                            source={iconMyPage}
                            style={{
                                width: size,
                                height: size,
                                tintColor: color,
                            }}
                        />
                    ),
                }}
            />
        </AppTab.Navigator>
    );
};

export default AppTabNavigator;
