import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import AppTabNavigator from './AppTabNavigator';

const AppStack = createStackNavigator();

const AppNavigator = () => {
    return (
        <AppStack.Navigator initialRouteName="AppTabNavigator">
            <AppStack.Screen
                name="AppTabNavigator"
                component={AppTabNavigator}
                options={{ headerLeft: null, headerShown: false }}
            />
        </AppStack.Navigator>
    );
};

export default AppNavigator;
