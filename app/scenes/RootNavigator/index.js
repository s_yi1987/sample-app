import React, { useMemo, useContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import AppNavigator from './AppNavigator';

import createMapSceneCoordinator from '../MapScene/coordinators';
import SceneFactoryContext from '../../contexts/SceneFactoryContext';

const RootStack = createStackNavigator();

const RootStackNavigator = () => {
    const factory = useContext(SceneFactoryContext);

    const StartScene = useMemo(() => factory.startScene(), [factory]);

    const MapScene = useMemo(
        () => createMapSceneCoordinator(factory.mapScene()),
        [factory],
    );

    return (
        <RootStack.Navigator mode="modal" initialRouteName="StartScene">
            <RootStack.Screen
                name="StartScene"
                component={StartScene}
                options={{ headerShown: false }}
            />
            <RootStack.Screen
                name="MapScene"
                component={MapScene}
                options={{
                    title: 'Find Nearest Branch',
                }}
            />
            <RootStack.Screen
                name="AppMainScene"
                component={AppNavigator}
                options={{ headerShown: false }}
            />
        </RootStack.Navigator>
    );
};

export default RootStackNavigator;
