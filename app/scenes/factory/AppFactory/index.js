import React from 'react';

import AppTabNavigator from '../../RootNavigator/AppTabNavigator';
import StartScene from '../../StartScene';

import apiAdaptedMapScene from '../../MapScene/apiAdapter';
import ReduxMapScene from '../../MapScene/injected';

import apiAdaptedRestaurantScene from '../../RestaurantScene/apiAdapter';
import ReduxRestaurantScene from '../../RestaurantScene/injected';

import Factory from '../Factory';

export default class AppFactory extends Factory {
    startScene = () => StartScene;

    mapScene = () => {
        return apiAdaptedMapScene(ReduxMapScene);
    };

    appTabNavigator = () => (props) => {
        return <AppTabNavigator {...props} factory={this} />;
    };

    restaurantScene = () => {
        return apiAdaptedRestaurantScene(ReduxRestaurantScene);
    };
}
