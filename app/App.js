import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { NavigationContainer } from '@react-navigation/native';

import DemoApi from './api/DemoApi';
import AppFactory from './scenes/factory/AppFactory';
import configureStore from './store/configure';

import RootNavigator from './scenes/RootNavigator';

import ApiLayerContext from './contexts/ApiLayerContext';
import SceneFactoryContext from './contexts/SceneFactoryContext';

const store = configureStore();

const App = () => {
    const [apiLayer] = useState(new DemoApi());
    const [factory] = useState(new AppFactory());

    return (
        <Provider store={store}>
            <ApiLayerContext.Provider value={apiLayer}>
                <SceneFactoryContext.Provider value={factory}>
                    <NavigationContainer>
                        <RootNavigator />
                    </NavigationContainer>
                </SceneFactoryContext.Provider>
            </ApiLayerContext.Provider>
        </Provider>
    );
};

export default App;
