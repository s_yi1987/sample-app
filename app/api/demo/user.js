const sampleUserMap = {
    demo1: {
        username: 'Demo1',
        password: 'Test1',
        userData: { name: 'Demo1', id: 'demo1', token: {} },
    },
    demo2: {
        username: 'Demo2',
        password: 'Test2',
        userData: { name: 'Demo2', id: 'demo2', token: {} },
    },
};

const signinDemoBase = async (userMap, { username, password }) => {
    const userIds = Object.keys(userMap);
    for (let i = 0; i < userIds.length; i += 1) {
        const user = userMap[userIds[i]];
        if (user && user.username === username && user.password === password) {
            return Promise.resolve(user.userData);
        }
    }

    return Promise.resolve({
        error: true,
        description: 'Invalid username or password',
    });
};

const signinDemo = (credentials) => signinDemoBase(sampleUserMap, credentials);

const fetchFavoritesBase = async (favoriteMap, { user }) => {};

const fetchFavorites = (params) => fetchFavoritesBase({}, params);

export { signinDemoBase, signinDemo, fetchFavoritesBase, fetchFavorites };
