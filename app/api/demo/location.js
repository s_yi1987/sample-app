const getCurrentPositionDemo = () =>
    Promise.resolve({ longitude: 127.0473, latitude: 37.5172 });

export default getCurrentPositionDemo;
