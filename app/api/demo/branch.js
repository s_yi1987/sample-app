import { getDistanceFromLngLat } from '../util';

import iconKoreanFood from '../../assets/images/iconKoreanFood/iconKoreanFood.png';
import iconSwedishFood from '../../assets/images/iconSwedishFood/iconSwedishFood.png';

const sampleBranchMap = {
    branch1: {
        longitude: 126.978,
        latitude: 37.5665,
        name: 'Seoul',
        id: 'branch1',
        foodCategories: [
            'Traditional',
            'Western',
            'Meat',
            'Seafood',
            'Chinese',
            'Pizza',
            'Fast Food',
        ],
    },
    branch2: {
        longitude: 18.0686,
        latitude: 59.3293,
        name: 'Stockholm',
        id: 'branch2',
        foodCategories: [
            'Traditional',
            'Meat',
            'Seafood',
            'Chinese',
            'Fast Food',
            'Vegetarian',
            'Pastries',
            'Healthy',
        ],
    },
};

const sampleRestaurantMap = {
    branch1: [
        {
            name: 'Korean Restaurant',
            image: iconKoreanFood,
            longitude: 126.977,
            latitude: 37.5664,
        },
    ],
    branch2: [
        {
            name: 'Swedish Restaurant',
            image: iconSwedishFood,
            longitude: 18.0685,
            latitude: 59.3292,
        },
    ],
};

const fetchNearestBranchDemoBase = async (branchMap, position) => {
    const branchKeys = Object.keys(branchMap);
    let shortestDistance = Number.MAX_SAFE_INTEGER;
    let nearestBranch;

    for (let i = 0; i < branchKeys.length; i += 1) {
        const currentBranch = branchMap[branchKeys[i]];
        const currentDistance = getDistanceFromLngLat(position, currentBranch);
        if (currentDistance <= shortestDistance) {
            nearestBranch = currentBranch;
            shortestDistance = currentDistance;
        }
    }

    return Promise.resolve(nearestBranch);
};

const fetchNearestBranchDemo = (position) =>
    fetchNearestBranchDemoBase(sampleBranchMap, position);

const fetchRestaurantsDemoBase = async (
    restaurantMap,
    { branchId, pageSize = 20 },
) => {
    const templates = restaurantMap[branchId] || [];

    if (templates.length > 0) {
        const restaurants = new Array(pageSize);
        for (let i = 0; i < pageSize; i += 1) {
            const templateIndex = i % templates.length;
            const template = templates[templateIndex];

            const restaurant = {
                name: `${template.name} ${i + 1}`,
                image: template.image,
                longitude: template.longitude,
                latitude: template.latitude,
            };
            restaurants[i] = restaurant;
        }
        return Promise.resolve(restaurants);
    }
    return Promise.resolve(templates);
};

const fetchRestaurantsDemo = (params) =>
    fetchRestaurantsDemoBase(sampleRestaurantMap, params);

export {
    fetchNearestBranchDemoBase,
    fetchNearestBranchDemo,
    fetchRestaurantsDemoBase,
    fetchRestaurantsDemo,
};
