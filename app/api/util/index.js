const getCombinedStyles = (baseStyle, ...styles) => {
    if (styles.length < 1) {
        return baseStyle;
    }

    const newStyle = Array.isArray(baseStyle) ? baseStyle : [baseStyle];
    for (let i = 0; i < styles.length; i += 1) {
        const style = styles[i];
        if (style) {
            if (Array.isArray(style)) {
                newStyle.push(...style);
            } else {
                newStyle.push(style);
            }
        }
    }
    return newStyle;
};

const getStyleValue = (style, key) => {
    let value;
    if (Array.isArray(style)) {
        for (let i = 0; i < style.length; i += 1) {
            const currentStyle = style[i];
            value = currentStyle[key] ? currentStyle[key] : value;
        }
    } else if (style) {
        value = style[key];
    }
    return value;
};

const getDistanceFromLngLat = (lngLat1, lngLat2) => {
    const { longitude: lon1, latitude: lat1 } = lngLat1;
    const { longitude: lon2, latitude: lat2 } = lngLat2;

    const R = 6371; // Radius of the earth in kilometers
    const dLat = degreesToRadius(lat2 - lat1);
    const dLon = degreesToRadius(lon2 - lon1);

    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(degreesToRadius(lat1)) *
            Math.cos(degreesToRadius(lat2)) *
            Math.sin(dLon / 2) *
            Math.sin(dLon / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in KM

    return d;
};

const degreesToRadius = (deg) => deg * (Math.PI / 180);

const getRegion = (longitude, latitude, distance) => {
    const oneDegreeOfLatitudeInMeters = 111.32 * 1000;

    const latitudeDelta = distance / oneDegreeOfLatitudeInMeters;
    const longitudeDelta =
        distance /
        (oneDegreeOfLatitudeInMeters * Math.cos(latitude * (Math.PI / 180)));

    return {
        latitude,
        longitude,
        latitudeDelta,
        longitudeDelta,
    };
};

const compareArrays = (arr1, arr2, cmp = (item1, item2) => item1 === item2) => {
    if (arr1 !== arr2) {
        if (arr1.length !== arr2.length) {
            return false;
        }
        for (let i = 0; i < arr1.length; i += 1) {
            if (!cmp(arr1[i], arr2[i])) {
                return false;
            }
        }
    }

    return true;
};

const withSideEffects = ({
    callback,
    pre = () => {},
    post = () => {},
}) => async (...params) => {
    pre(...params);
    const res = await Promise.resolve(callback(...params));
    post(...params);

    return res;
};

export {
    getCombinedStyles,
    getStyleValue,
    getDistanceFromLngLat,
    getRegion,
    compareArrays,
    withSideEffects,
};
