import Api from './Api';
import { fetchNearestBranchDemo, fetchRestaurantsDemo } from './demo/branch';
import { signinDemo } from './demo/user';
import getCurrentPositionDemo from './demo/location';

export default class DemoApi extends Api {
    constructor() {
        super();

        this.fetchNearestBranch = fetchNearestBranchDemo;
        this.fetchRestaurants = fetchRestaurantsDemo;
        this.signin = signinDemo;
        this.getCurrentPosition = getCurrentPositionDemo;
    }
}
