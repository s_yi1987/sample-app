import React from 'react';
import { Dimensions } from 'react-native';
import { render, waitFor, fireEvent } from '@testing-library/react-native';

import RestaurantScene from '../../app/scenes/RestaurantScene';

describe('RestaurantScene', () => {
    // Right now there is a bug which triggers an annoying warning when having more than one await inside an it block
    // https://github.com/callstack/react-native-testing-library/issues/379
    const originalError = console.error;

    beforeAll(() => {
        console.error = jest.fn();
    });

    afterAll(() => {
        console.error = originalError;
    });

    describe('tabs', () => {
        it('should render the food categories correctly', async () => {
            const location = { longitude: 127.0473, latitude: 37.5172 };
            const branch = {
                longitude: 126.978,
                latitude: 37.5665,
                name: 'Seoul',
                id: 'branch1',
                foodCategories: [
                    'Traditional',
                    'Western',
                    'Meat',
                    'Seafood',
                    'Chinese',
                    'Pizza',
                    'Fast Food',
                ],
            };
            const fetchCategoryData = ({ index }) =>
                Promise.resolve([
                    {
                        label: 'Test Restaurant',
                        source: null,
                        subLabel: '',
                        key: `test${index}`,
                    },
                ]);

            const { getAllByText, queryAllByText, update } = render(
                <RestaurantScene
                    location={location}
                    branch={branch}
                    fetchCategoryData={fetchCategoryData}
                />,
            );

            await waitFor(() => getAllByText('Test Restaurant'));

            expect(getAllByText('Traditional').length).toBeGreaterThan(0);
            expect(getAllByText('Western').length).toBeGreaterThan(0);
            expect(getAllByText('Meat').length).toBeGreaterThan(0);
            expect(getAllByText('Seafood').length).toBeGreaterThan(0);
            expect(getAllByText('Chinese').length).toBeGreaterThan(0);
            expect(getAllByText('Pizza').length).toBeGreaterThan(0);
            expect(getAllByText('Fast Food').length).toBeGreaterThan(0);
            expect(queryAllByText('Incorrect').length).toBe(0);

            // Got rid of Chinese, Seafood and Pizza
            const branch2 = {
                longitude: 126.978,
                latitude: 37.5665,
                name: 'Seoul',
                id: 'branch2',
                foodCategories: ['Traditional', 'Western', 'Meat', 'Fast Food'],
            };

            update(
                <RestaurantScene
                    branch={branch2}
                    fetchCategoryData={fetchCategoryData}
                    location={location}
                />,
            );
            await waitFor(() => getAllByText('Test Restaurant'));

            expect(getAllByText('Traditional').length).toBeGreaterThan(0);
            expect(getAllByText('Western').length).toBeGreaterThan(0);
            expect(getAllByText('Meat').length).toBeGreaterThan(0);

            expect(queryAllByText('Seafood').length).toBe(0);
            expect(queryAllByText('Chinese').length).toBe(0);
            expect(queryAllByText('Pizza').length).toBe(0);

            expect(getAllByText('Fast Food').length).toBeGreaterThan(0);
        });
    });

    describe('pages', () => {
        it('should render the pages correctly', async () => {
            const location = { longitude: 127.0473, latitude: 37.5172 };
            const branch = {
                longitude: 126.978,
                latitude: 37.5665,
                name: 'Seoul',
                id: 'branch1',
                foodCategories: [
                    'Traditional',
                    'Western',
                    'Meat',
                    'Seafood',
                    'Chinese',
                    'Pizza',
                    'Fast Food',
                ],
            };
            const fetchCategoryData = ({ index }) =>
                Promise.resolve([
                    {
                        label: 'Test Restaurant',
                        source: null,
                        subLabel: '',
                        key: `test${index}`,
                    },
                ]);

            const { getAllByText, update } = render(
                <RestaurantScene
                    location={location}
                    branch={branch}
                    fetchCategoryData={fetchCategoryData}
                />,
            );

            await waitFor(() => getAllByText('Test Restaurant'));

            const fetchCategoryData2 = ({ index }) =>
                Promise.resolve([
                    {
                        label: 'Hello',
                        source: null,
                        subLabel: '',
                        key: `hello${index}`,
                    },
                ]);
            update(
                <RestaurantScene
                    branch={branch}
                    fetchCategoryData={fetchCategoryData2}
                    location={location}
                />,
            );

            await waitFor(() => getAllByText('Hello'));

            expect(getAllByText('Hello').length).toBeGreaterThan(0);
        });
    });

    describe('simulate page swipe', () => {
        it('should load pages correctly', async () => {
            const location = { longitude: 127.0473, latitude: 37.5172 };
            const branch = {
                longitude: 126.978,
                latitude: 37.5665,
                name: 'Seoul',
                id: 'branch1',
                foodCategories: ['Traditional', 'Western', 'Meat'],
            };

            const fetchCategoryData = ({ index }) => {
                let result = {
                    label: 'Test Restaurant',
                    source: null,
                    subLabel: '',
                    key: `test${index}`,
                };

                if (index === 1) {
                    result = {
                        label: 'Western Restaurant',
                        source: null,
                        subLabel: '',
                        key: `western${index}`,
                    };
                }
                return Promise.resolve([result]);
            };
            const fetchCategorySpy = jest.fn(fetchCategoryData);
            const deviceWidth = Dimensions.get('window').width;
            const deviceHeight = Dimensions.get('window').height;

            const { getAllByText, queryAllByText, getByTestId } = render(
                <RestaurantScene
                    location={location}
                    branch={branch}
                    fetchCategoryData={fetchCategorySpy}
                    contentProps={{
                        testID: 'flatList',
                    }}
                    pageDimensions={{
                        width: deviceWidth,
                        height: deviceHeight,
                    }}
                    createOnScrollHandler={({ onScrollX }) => (e) => {
                        onScrollX([e.nativeEvent.contentOffset.x]);
                    }}
                />,
            );

            await waitFor(() => getAllByText('Test Restaurant'));

            expect(queryAllByText('Western Restaurant').length).toBe(0);

            const flatList = getByTestId('flatList');
            fireEvent.scroll(flatList, {
                nativeEvent: {
                    contentOffset: {
                        x: deviceWidth,
                        y: 0,
                    },
                    contentSize: {
                        height: deviceHeight,
                        width: deviceWidth * 3,
                    },
                    layoutMeasurement: {
                        height: deviceHeight,
                        width: deviceWidth,
                    },
                },
            });

            await waitFor(() => getAllByText('Western Restaurant'));
            expect(fetchCategorySpy.mock.calls.length).toBe(2);
        });
    });
});
