import React from 'react';
import { render, waitFor, fireEvent } from '@testing-library/react-native';

import MapScene from '../../app/scenes/MapScene';

describe('MapScene', () => {
    describe('callbacks', () => {
        it('should be called properly with the right parameters', async () => {
            const originalError = console.error;
            console.error = jest.fn();

            const getCurrentPosition = () =>
                Promise.resolve({ longitude: 127.0473, latitude: 37.5172 });

            const fetchNearestBranch = () =>
                Promise.resolve({
                    longitude: 126.978,
                    latitude: 37.5665,
                    name: 'Test',
                    id: 'testBranch',
                    foodCategories: ['Test A', 'Test B'],
                });

            const onBranchSet = jest.fn();
            const setLocation = jest.fn();

            const { getByText } = render(
                <MapScene
                    fetchNearestBranch={fetchNearestBranch}
                    getCurrentPosition={getCurrentPosition}
                    onBranchSet={onBranchSet}
                    setLocation={setLocation}
                    debounceTime={0}
                />,
            );

            await waitFor(() => getByText('Select'));

            expect(onBranchSet.mock.calls.length).toBe(0);
            fireEvent.press(getByText('Select'));
            await waitFor(() => expect(onBranchSet).toHaveBeenCalled());

            expect(onBranchSet.mock.calls[0][0].selectedBranch.name).toBe(
                'Test',
            );
            expect(onBranchSet.mock.calls[0][0].selectedBranch.id).toBe(
                'testBranch',
            );

            expect(setLocation.mock.calls.length).toBe(1);
            expect(setLocation.mock.calls[0][0].longitude).toBe(127.0473);
            expect(setLocation.mock.calls[0][0].latitude).toBe(37.5172);

            console.error = originalError;
        });
    });
});
