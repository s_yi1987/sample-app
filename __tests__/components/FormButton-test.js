import { View } from 'react-native';
import React from 'react';

// Note: test renderer must be required after react-native.
import renderer from 'react-test-renderer';
import { render, fireEvent } from '@testing-library/react-native';

import FormButton from '../../app/components/FormButton';

it('renders correctly', () => {
    const tree = renderer.create(<FormButton label="test" />).toJSON();
    expect(tree).toMatchSnapshot();
});

it('renders leftView and rightView correctly', () => {
    const tree = renderer
        .create(
            <FormButton
                label="test2"
                leftView={
                    <View
                        style={{
                            width: 10,
                            height: 10,
                            backgroundColor: 'blue',
                        }}
                    />
                }
                rightView={
                    <View
                        style={{
                            width: 10,
                            height: 10,
                            backgroundColor: 'green',
                        }}
                    />
                }
            />,
        )
        .toJSON();
    expect(tree).toMatchSnapshot();
});

it('it calls onPress correctly', () => {
    const onPress = jest.fn();
    const { getByText, update } = render(
        <FormButton onPress={onPress} label="Press" />,
    );

    expect(onPress.mock.calls.length).toBe(0);
    fireEvent.press(getByText('Press'));

    expect(onPress.mock.calls.length).toBe(1);

    fireEvent.press(getByText('Press'));
    expect(onPress.mock.calls.length).toBe(2);

    const onPress2 = jest.fn();
    update(<FormButton onPress={onPress2} label="Press2" />);

    expect(onPress2.mock.calls.length).toBe(0);
    fireEvent.press(getByText('Press2'));

    expect(onPress.mock.calls.length).toBe(2);
    expect(onPress2.mock.calls.length).toBe(1);
});
