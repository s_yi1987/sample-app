import { getDistanceFromLngLat } from '../app/api/util';

describe('getDistanceFromLngLat', () => {
    it('Berlin should be closer to Stockholm', () => {
        const seoulPos = { longitude: 126.977, latitude: 37.5664 };
        const stockholmPos = { longitude: 18.0685, latitude: 59.3292 };
        const berlinPos = { longitude: 13.405, latitude: 52.52 };

        const seoulStockholmDist = getDistanceFromLngLat(
            seoulPos,
            stockholmPos,
        );

        const berlinStockholmDist = getDistanceFromLngLat(
            berlinPos,
            stockholmPos,
        );

        expect(seoulStockholmDist).toBeGreaterThan(berlinStockholmDist);
    });

    it('Gangnam should be closer to Seoul', () => {
        const gangnamPos = { longitude: 127.0473, latitude: 37.5172 };
        const seoulPos = { longitude: 126.977, latitude: 37.5664 };
        const stockholmPos = { longitude: 18.0685, latitude: 59.3292 };

        const gangnamSeoulDist = getDistanceFromLngLat(gangnamPos, seoulPos);

        const stockholmSeoulDist = getDistanceFromLngLat(
            stockholmPos,
            seoulPos,
        );

        expect(gangnamSeoulDist).toBeLessThan(stockholmSeoulDist);
    });
});
